﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Classes.PessoaJuridica
{
    public class PJDTO
    {
        public int ID { get; set; }

        public string NomeSocial{ get; set; }

        public string  NomeFantasia { get; set; }

        public string  CNPJ { get; set; }

        public string ContatoUm { get; set; }

        public string TelefoneUm { get; set; }

        public string ContatoDois { get; set; }

        public string TelefoneDois { get; set; }

        public string  Rua { get; set; }

        public string  Bairro { get; set; }

        public string  CEP { get; set; }

        public string  Estado { get; set; }

        public string  Municipio { get; set; }

        public string  Observacoes { get; set; }



    }
}
