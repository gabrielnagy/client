﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Classes.PessoaFisica
{
    public class PFDTO
    {
        public int ID { get; set; }

        public string NomeUsuario { get; set; }

        public string CPF { get; set; }

        public string RG { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public string Rua { get; set; }

        public string Bairro { get; set; }

        public string CEP { get; set; }

        public string Estado { get; set; }

        public string Cidade { get; set; }

        public string Observacoes { get; set; }
    }
}
