﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Classes.PessoaFisica
{
    class PFBusiness
    {
        PFDatabase db = new PFDatabase();
        public int Salvar(PFDTO dto)
        {
            return(db.Salvar(dto));
        }

        public List<PFDTO> ConsultarTudo(PFDTO dto)
        {
            return (db.Consultar(dto));
        }



    }
}
