﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Classes.Base;

namespace WindowsFormsApp1.Classes.PessoaFisica
{
    class PFDatabase
    {
        Database db = new Database();
        public int Salvar(PFDTO dto)
        {
            string script = "INSERT INTO tb_pessoa_fisica (nm_pf, ds_cpf, ds_rg, ds_telefone, ds_email, ds_rua, ds_bairro, ds_cep, ds_estado, ds_cidade, ds_observacoes) VALUES (@nm_pf , @ds_cpf, @ds_rg, @ds_telefone, @ds_email, @ds_rua, @ds_bairro, @ds_cep, @ds_estado, @ds_cidade, @ds_observacoes)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_pf", dto.NomeUsuario));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_observacoes", dto.Observacoes));

            int id = db.ExecuteInsertScriptWithPk(script, parms);
            return id;

        }

        public PFDTO Consultar(PFDTO dto)
        {
            string script = "Select * FROM tb_pessoa_fisica";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            while (reader.Read())
            {
                dto.ID = reader.GetInt32("id_pf");
                dto.NomeUsuario= reader.GetString("nm_pf");
                dto.CPF = reader.GetString("ds_cpf");
                dto.RG = reader.GetString("ds_rg");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Telefone = reader.GetString("ds_email");
                dto.Rua = reader.GetString("ds_rua");
                dto.Bairro = reader.GetString("ds_bairro");
                dto.CEP = reader.GetString("ds_cep");
                dto.Estado = reader.GetString("ds_estadp");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Observacoes = reader.GetString("ds_observacoes");
            }

            reader.Close();

            return dto;
        }
    }
}
