﻿using Correios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Classes.CorreioCEP
{
    public class Localizar
    {

        MovimentoDadosCep dto = new MovimentoDadosCep();
        string mensagem;
        public MovimentoDadosCep LocalizarCEP(MovimentoDadosCep dto)
        {
            try
            {

            
            if(dto.CEP == string.Empty)
            {
                throw new ArgumentException("o campo esta vazio");
            }

                CorreiosApi correios = new CorreiosApi();

                var retorno = correios.consultaCEP(dto.CEP);

                dto.Estado = retorno.uf;
                dto.Cidade = retorno.cidade;
                dto.Bairro = retorno.bairro;
                dto.Rua = retorno.end;
        
            return dto;
            }

            catch (Exception er)
            {
                throw new ArgumentException(er.Message);
            }
            
        }
    }
}
