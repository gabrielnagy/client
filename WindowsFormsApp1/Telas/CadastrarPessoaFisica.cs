﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Classes.CorreioCEP;
using WindowsFormsApp1.Classes.PessoaFisica;

namespace WindowsFormsApp1.Telas
{
    public partial class CadastrarPessoaFisica : UserControl
    {
        public CadastrarPessoaFisica()
        {
            InitializeComponent();
        }
        Localizar Correio = new Localizar();

        private void textBox1_Leave(object sender, EventArgs e)
        {
            MovimentoDadosCep dto = new MovimentoDadosCep();
            dto.CEP = txtCEP.Text;
            dto = Correio.LocalizarCEP(dto);

            txtBairro.Text = dto.Bairro;
            txtCidade.Text = dto.Cidade;
            txtEstado.Text = dto.Estado;
            txtRua.Text = dto.Rua;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            PFDTO dto = new PFDTO();

            dto.NomeUsuario = txtNome.Text;
            dto.Observacoes = txtObs.Text;
            dto.RG = txtRG.Text;
            dto.Rua = txtRua.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Bairro = txtBairro.Text;
            dto.CEP = txtCEP.Text;
            dto.Cidade = txtCidade.Text;
            dto.CPF = txtCPF.Text;
            dto.Estado = txtEstado.Text;

            PFBusiness buss = new PFBusiness();
            buss.Salvar(dto);

            MessageBox.Show("Salvo com sucesso");
        }

        private void btnConfirmar_Click_1(object sender, EventArgs e)
        {

        }
    }
}
